// Rdsvs reads data records from a Valeport miniSVS (Sound Velocity Sensor) and
// publishes them to a NATS Streaming Server
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	svs "bitbucket.org/uwaploe/go-svs"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: rdsvs [options]

Read data from the MuST Sound Velocity Sensor and publish to a NATS Streaming Server.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL   string = "nats://localhost:4222"
	clusterID string = "must-cluster"
	svDev     string
	svSubject string
	svBaud    int = 19200
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&svDev, "sv-dev", lookupEnvOrString("SV_DEVICE", svDev),
		"SV sensor device")
	flag.IntVar(&svBaud, "sv-baud", lookupEnvOrInt("SV_BAUD", svBaud),
		"SV sensor baud rate")
	flag.StringVar(&svSubject, "sub", lookupEnvOrString("SV_SUBJECT", svSubject),
		"Subject name for SV data")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	parseCmdLine()

	if svDev == "" {
		log.Fatal("SV_DEVICE not set, aborting")
	}

	sc, err := stan.Connect(clusterID, "sv-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	// SV_DEVICE can either be a serial device name or an IPADDR:PORT
	// network address.
	var (
		p Port
	)

	if strings.Contains(svDev, ":") {
		p, err = NetworkPort(svDev)
	} else {
		p, err = SerialPort(svDev, svBaud)
	}

	if err != nil {
		log.Fatalf("Cannot open SVS device: %v", err)
	}
	defer p.Close()

	// Assume the device is in streaming mode
	dev := svs.New(p, true)
	err = dev.Stop()
	if err != nil {
		log.Fatalf("Cannot access SVS: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	err = dev.Start(svs.Rate1Hz)
	if err != nil {
		log.Fatalf("Cannot start streaming mode: %v", err)
	}

	log.Printf("Starting SVS monitor %s", Version)
	ch, _ := dev.ReadStream(ctx)
	for rec := range ch {
		b, err := msgpack.Marshal(&rec)
		if err != nil {
			log.Printf("Msgpack encoding failed: %v", err)
		} else {
			err = sc.Publish(svSubject, b)
			if err != nil {
				log.Printf("Publish failed: %v", err)
			}
		}
	}

	es, ds := dev.Errors()
	log.Printf("Data stream closed; errors=%d discards=%d", es, ds)
}
