module bitbucket.org/uwaploe/rdsvs

require (
	bitbucket.org/mfkenney/go-nmea v1.1.0 // indirect
	bitbucket.org/uwaploe/go-svs v0.2.1
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.2
	github.com/nats-io/nkeys v0.0.2 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	golang.org/x/sys v0.0.0-20190411185658-b44545bcd369 // indirect
)

go 1.13
